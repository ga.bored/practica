import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    public json: any;
    public array_nUsuarios: any;
    public length: any;

    // Definimos tres variables: 1 -> una de tipo any donde almacenaremos el JSON.  2-> un Array que crearemos despues en base a la longitud de la lista que querramos accesar. 3 -> la variable donde almacenaremos la longitud de nuestra lista JSON


  constructor() {

    // No se si este 100% correcto hacerlo en el constructor pero así jala :)

    // Declaración del JSON
    this.json= {
      "empresa" : "La Aceitera",
      "sistema" : {
          "nombre": "Contabilidad",
          "área" : "administración",
      "usuarios" : [
                  {
                      "nombre" : "Alberto",
                      "apellidos" : "Carreón Dominguez",
                      "edad" : 25,
                      "foto" : "https://gpluseurope.com/wp-content/uploads/Mauro-profile-picture.jpg",
                      "color": "green"
                  },
                  {
                      "nombre" : "Ana",
                      "apellidos" : "Gutierrez Gómez",
                      "edad" : 28,
                      "foto" : "https://cdn-makehimyours.pressidium.com/wp-content/uploads/2016/11/Depositphotos_9830876_l-2015Optimised.jpg",
                      "color": "cyan"
                  },
                  {
                      "nombre" : "Luis Roberto",
                      "apellidos" : "Rentería Aguilar",
                      "edad" : 35,
                      "foto" : "https://linksys.i.lithium.com/t5/image/serverpage/image-id/17360i094A9CC74F39EE12/image-size/original?v=1.0&px=-1",
                      "color": "blue"
                  },
                  {
                      "nombre" : "Diana Isela",
                      "apellidos" : "Herrera Valdéz",
                      "edad" : 22,
                      "foto" : "http://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-112.jpg",
                      "color": "red"
                  },
                  {
                      "nombre" : "Alma Rosa",
                      "apellidos" : "Velazquez López",
                      "edad" : 28,
                      "foto" : "http://lavinephotography.com.au/wp-content/uploads/2017/01/WEBRebecca-Bryant_7296-.jpg",
                      "color": "yellow"
                  },
                  {
                      "nombre" : "Jorge Humberto",
                      "apellidos" : "Salazar Hernández",
                      "edad" : 62,
                      "foto" : "http://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-107.jpg",
                      "color": "orange"
                  },
                  {
                    "nombre" : "Bicho",
                    "apellidos" : "Ronaldo",
                    "edad" : 77,
                    "foto" : "http://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-107.jpg",
                    "color": "orange"
                }
              ],
          "actualizado" : true
      }
    };
    
    // con esta función que saque de internet, podemos medir el length de nuestra lista JSON y poder almacenarla en una variable
    this.length = Object.keys(this.json.sistema.usuarios).length;
    // con esta función estamos creando el array que sera de largo "length" para poder acceder a los valores de cada elemento respectivamente
    // es decir que con esto podemos acceder al elemento "i" de un for de nuestro array json
    this.array_nUsuarios = Array.apply(null, {length: this.length}).map(Number.call, Number)
  }

}
